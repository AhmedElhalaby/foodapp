<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login','AuthController@login');
    Route::post('signup','AuthController@register');
    Route::post('forget_password','AuthController@forget_password');
    Route::post('reset_password','AuthController@reset_password');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('me','AuthController@show');
        Route::get('vendor_detail','AuthController@vendor_detail');
        Route::post('refresh','AuthController@refresh');
        Route::post('vendor','AuthController@vendor');
        Route::post('update','AuthController@update');
        Route::get('resend_verify', 'AuthController@resend_verify');
        Route::post('verify', 'AuthController@verify');
        Route::post('change_password','AuthController@change_password');
        Route::post('logout','AuthController@logout');
    });
});
Route::group([
    'middleware' => 'auth:api'
], function() {
    Route::group([
        'prefix' => 'notifications',
    ], function() {
        Route::get('/', 'NotificationController@index');
        Route::post('/read', 'NotificationController@read');
        Route::post('/read/all', 'NotificationController@read_all');
    });
    Route::group([
        'prefix' => 'advertisements',
    ], function() {
        Route::get('/', 'AdvertisementController@index');
        Route::get('products', 'AdvertisementController@products');
        Route::get('mine', 'AdvertisementController@mine');
        Route::get('show', 'AdvertisementController@show');
        Route::post('store', 'AdvertisementController@store');
    });
    Route::group([
        'prefix' => 'products',
    ], function() {
        Route::get('/', 'ProductController@index');
        Route::post('store', 'ProductController@store');
        Route::get('show', 'ProductController@show');
        Route::post('update', 'ProductController@update');
        Route::post('destroy', 'ProductController@destroy');
        Route::post('media/upload', 'ProductController@upload_media');
        Route::post('media/destroy', 'ProductController@destroy_media');
    });
    Route::group([
        'prefix' => 'home',
    ], function() {
        Route::get('vendors', 'HomeController@vendors');
        Route::get('vendor', 'HomeController@vendor');
        Route::get('faqs','HomeController@faqs');
        Route::post('/subscribe','HomeController@subscribe');
        Route::group([
            'prefix' => 'tickets',
        ], function() {
            Route::get('/','HomeController@tickets');
            Route::post('/add','HomeController@addTicket');
            Route::post('/add/response','HomeController@add_response');
            Route::get('/show','HomeController@show_ticket');
        });
    });
    Route::group([
        'prefix' => 'orders',
    ], function() {
        Route::get('/', 'OrderController@index');
        Route::get('show', 'OrderController@show');
        Route::post('store', 'OrderController@store');
        Route::post('update', 'OrderController@update');
        Route::post('review', 'OrderController@review');
    });

});
Route::get('install','HomeController@install');

