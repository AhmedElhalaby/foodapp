<?php

namespace App\Http\Resources\Api\Advertisement;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AdvertisementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $Objects = array();
        $Objects['id'] = $this->getId();
        $Objects['vendor_id'] = ($this->user->vendor)?$this->user->vendor->getId():null;
        $Objects['type'] = $this->getType();
        $Objects['ref_id'] = $this->getRefId();
        $Objects['media'] = $this->getMedia();
        $Objects['status'] = $this->getStatus();
        return $Objects;
    }
}
