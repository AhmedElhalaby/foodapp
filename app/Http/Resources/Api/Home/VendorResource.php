<?php

namespace App\Http\Resources\Api\Home;

use App\Helpers\Constant;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class VendorResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['id'] = $this->getId();
        $Object['name'] = $this->getName();
        $Object['mobile'] = $this->user->getMobile();
        $Object['type'] = $this->getType();
        $Object['delivery_price'] = $this->getDeliveryPrice();
        $Object['is_open'] = $this->isIsOpen();
        $Object['products_count'] = $this->products()->count();
        $Object['lat'] = $this->user->getLat();
        $Object['lng'] = $this->user->getLng();
        $Orders = Order::where('vendor_id',$this->getId())->where('status',Constant::ORDER_STATUSES['Completed'])->pluck('id');
        $Object['rate'] = Review::whereIn('ref_id',$Orders)->where('type',Constant::REVIEW_TYPES['Order'])->avg('rate');
        $Object['avatar'] = $this->user->getAvatar();
        return $Object;
    }

}
