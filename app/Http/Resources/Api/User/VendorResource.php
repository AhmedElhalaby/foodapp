<?php

namespace App\Http\Resources\Api\User;

use App\Helpers\Constant;
use App\Models\Notification;
use App\Models\Subscription;
use App\Models\VendorSubscription;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class VendorResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['id'] = $this->getId();
        $Object['name'] = $this->getName();
        $Object['email'] = $this->user->getEmail();
        $Object['type'] = $this->getType();
        $Object['delivery_price'] = $this->getDeliveryPrice();
        $Object['identity_number'] = $this->getIdentityNumber();
        $Object['manager_name'] = $this->getManagerName();
        $Object['commercial_registration_no'] = $this->getCommercialRegistrationNo();
        $Object['avatar'] = $this->user->getAvatar();
        $Object['is_open'] = $this->isIsOpen();
        $Subscription = VendorSubscription::where('vendor_id',$this->getId())->where('expire_at','>',Carbon::today()->format('Y-d-m'))->where('status',Constant::SUBSCRIPTION_STATUSES['Approved'])->first();
        $Object['is_subscribed'] = $Subscription?true:false;
        return $Object;
    }

}
