<?php

namespace App\Http\Resources\Api\Product;

use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class VendorResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['id'] = $this->getId();
        $Object['name'] = $this->getName();
        $Object['delivery_price'] = $this->getDeliveryPrice();
        $Object['lat'] = $this->user->getLat();
        $Object['lng'] = $this->user->getLng();
        return $Object;
    }

}
