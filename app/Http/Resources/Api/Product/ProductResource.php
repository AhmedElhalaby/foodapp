<?php

namespace App\Http\Resources\Api\Product;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['id'] = $this->getId();
        $Object['Vendor'] = new VendorResource($this->vendor);
        $Object['name'] = $this->getName();
        $Object['Category'] = new CategoryResource($this->category);
        $Object['SubCategory'] =  new CategoryResource($this->sub_category);
        $Object['price'] = $this->getPrice();
        $Object['offer_price'] = $this->getOfferPrice();
        $Object['description'] = $this->getDescription();
        $Object['preparation_time'] = $this->getPreparationTime();
        $Object['rate'] = $this->rate();
        $Object['Media'] = MediaResource::collection($this->media);
        return $Object;
    }

}
