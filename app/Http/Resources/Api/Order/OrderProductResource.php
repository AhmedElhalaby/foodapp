<?php

namespace App\Http\Resources\Api\Order;

use App\Http\Resources\Api\Product\CategoryResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['id'] = $this->getProductId();
        $Object['quantity'] = $this->getQuantity();
        $Object['name'] = $this->getName();
        $Object['Category'] = new CategoryResource($this->category);
        $Object['SubCategory'] =  new CategoryResource($this->sub_category);
        $Object['price'] = $this->getPrice();
        $Object['offer_price'] = $this->getOfferPrice();
        $Object['description'] = $this->getDescription();
        $Object['preparation_time'] = $this->getPreparationTime();
        $Object['rate'] = 0;
        $Object['image'] = $this->getImage();
        $Object['note'] = $this->getNote();
        return $Object;
    }

}
