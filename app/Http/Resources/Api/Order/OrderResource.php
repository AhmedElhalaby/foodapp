<?php

namespace App\Http\Resources\Api\Order;

use App\Http\Resources\Api\General\ReviewResource;
use App\Http\Resources\Api\Home\UserResource;
use App\Http\Resources\Api\Home\VendorResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['id'] = $this->getId();
        $Object['VendorDetails'] = new VendorResource($this->vendor);
        $Object['UserDetails'] = new UserResource($this->user);
        $Object['recipient_name'] = $this->getRecipientName();
        $Object['address'] = $this->getAddress();
        $Object['lat'] = $this->getLat();
        $Object['lng'] = $this->getLng();
        $Object['products_cost'] = $this->getProductsCost();
        $Object['delivery_cost'] = $this->getDeliveryCost();
        $Object['order_date'] = $this->getOrderDate();
        $Object['order_time'] = $this->getOrderTime();
        $Object['total_cost'] = $this->getTotalCost();
        $Object['status'] = $this->getStatus();
        $Object['notes'] = $this->getNotes();
        $Object['Review'] = new ReviewResource($this->review);
        $Object['created_at'] = Carbon::parse($this->created_at);
        $Object['OrderProducts'] = OrderProductResource::collection($this->order_products);
        return $Object;
    }

}
