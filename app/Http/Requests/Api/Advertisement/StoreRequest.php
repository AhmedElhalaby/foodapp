<?php

namespace App\Http\Requests\Api\Advertisement;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Advertisement\AdvertisementResource;
use App\Models\Advertisement;
use App\Traits\ResponseTrait;

class StoreRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'=>'required|numeric',
            'media'=>'required|mimes:jpeg,jpg,png',
            'ref_id'=>'required|numeric'
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Object = new Advertisement();
        $Object->setUserId($logged->getId());
        $Object->setType($this->type);
        $Object->setRefId($this->ref_id);
        $Object->setMedia($this->file('media'));
        $Object->save();
        $Object->refresh();
        return $this->successJsonResponse([],new AdvertisementResource($Object),'Advertisement');
    }
}
