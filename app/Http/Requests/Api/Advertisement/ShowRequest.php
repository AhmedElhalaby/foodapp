<?php

namespace App\Http\Requests\Api\Advertisement;

use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Advertisement\AdvertisementResource;
use App\Models\Advertisement;
use App\Traits\ResponseTrait;

class ShowRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'advertisement_id'=>'required|exists:advertisements,id'
        ];
    }

    public function persist()
    {
        return $this->successJsonResponse([],new AdvertisementResource((new Advertisement())->find($this->advertisement_id)),'Advertisement');
    }
}
