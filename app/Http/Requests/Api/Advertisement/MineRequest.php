<?php

namespace App\Http\Requests\Api\Advertisement;

use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Advertisement\AdvertisementResource;
use App\Models\Advertisement;
use App\Traits\ResponseTrait;

class MineRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function persist()
    {
        $Advertisements = Advertisement::where('user_id',auth()->user()->getId())->orderBy('created_at','desc')->paginate($this->per_page?:10);
        return $this->successJsonResponse([],AdvertisementResource::collection($Advertisements->items()),'Advertisements',$Advertisements);
    }
}
