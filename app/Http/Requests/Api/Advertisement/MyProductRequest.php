<?php

namespace App\Http\Requests\Api\Advertisement;

use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Advertisement\AdvertisementResource;
use App\Http\Resources\Api\Advertisement\ProductResource;
use App\Models\Advertisement;
use App\Models\Product;
use App\Traits\ResponseTrait;

class MyProductRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function persist()
    {
        $Products = Product::where('user_id',auth()->user()->getId())->orderBy('created_at','desc')->get();
        return $this->successJsonResponse([],ProductResource::collection($Products),'Products');
    }
}
