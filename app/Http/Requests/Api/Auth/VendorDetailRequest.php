<?php

namespace App\Http\Requests\Api\Auth;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\User\UserResource;
use App\Http\Resources\Api\User\VendorResource;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class VendorDetailRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->getType() == Constant::USER_TYPE['Vendor'];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function attributes()
    {
        return [];
    }
    public function persist()
    {
        $logged = auth()->user();
        return $this->successJsonResponse( [],new VendorResource($logged->vendor),'Vendor');

    }
}
