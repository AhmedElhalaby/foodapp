<?php

namespace App\Http\Requests\Api\Auth;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\User\UserResource;
use App\Http\Resources\Api\User\VendorResource;
use App\Models\Vendor;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class VendorRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255,',
            'identity_number' => 'required|string|min:6',
            'commercial_registration_no' => 'string|required_if:type,'.Constant::VENDOR_TYPE['Gallery'],
            'manager_name' => 'string|required_if:type,'.Constant::VENDOR_TYPE['Gallery'],
            'delivery_price' => 'required|numeric',
            'type' => 'required|in:'.Constant::VENDOR_TYPE_RULES,

        ];
    }
    public function attributes()
    {
        return [];
    }
    public function persist()
    {
        $logged = auth()->user();
        $Vendor = $logged->vendor;
        $Vendor->setName($this->name);
        $Vendor->setType($this->type);
        $Vendor->setDeliveryPrice($this->delivery_price);
        $Vendor->setIdentityNumber($this->identity_number);
        if($this->type==Constant::VENDOR_TYPE['Gallery']){
            $Vendor->setManagerName($this->manager_name);
            $Vendor->setCommercialRegistrationNo($this->commercial_registration_no);
        }
        if ($this->filled('is_open')){
            $Vendor->setIsOpen($this->is_open);
            $Vendor->save();
        }
        $Vendor->save();
        return $this->successJsonResponse( [__('messages.updated_successful')],new VendorResource($Vendor),'Vendor');

    }
}
