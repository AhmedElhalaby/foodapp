<?php

namespace App\Http\Requests\Api\Product;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Product\ProductResource;
use App\Models\Media;
use App\Models\Product;
use App\Traits\ResponseTrait;

class DestroyImageRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id'=>'required|exists:products,id',
            'media_id'=>'required|exists:media,id',
        ];
    }

    public function persist()
    {
        $Object = (new Product())->find($this->product_id);
        $Media = (new Media())->find($this->media_id);
        $Media->delete();
        return $this->successJsonResponse([__('admin.messages.deleted_successfully')],new ProductResource($Object),'Product');
    }
}
