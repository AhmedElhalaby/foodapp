<?php

namespace App\Http\Requests\Api\Product;

use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Product\ProductResource;
use App\Models\Product;
use App\Traits\ResponseTrait;

class IndexRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function persist()
    {
        $Objects = new Product();
        if($this->filled('user_id')){
            $Objects = $Objects->where('user_id',$this->user_id);
        }
        if($this->filled('vendor_id')){
            $Objects = $Objects->where('vendor_id',$this->vendor_id);
        }
        if($this->filled('category_id')){
            $Objects = $Objects->where('category_id',$this->category_id);
        }
        if($this->filled('sub_category_id')){
            $Objects = $Objects->where('sub_category_id',$this->sub_category_id);
        }
        if($this->filled('q')){
            $Objects = $Objects->where('name','LIKE',"%{$this->q}%");
        }
        $Objects = $Objects->orderBy('created_at','desc')->paginate($this->per_page?:10);
        return $this->successJsonResponse([],ProductResource::collection($Objects->items()),'Products',$Objects);
    }
}
