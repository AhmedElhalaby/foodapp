<?php

namespace App\Http\Requests\Api\Product;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Product\ProductResource;
use App\Models\Media;
use App\Models\Product;
use App\Traits\ResponseTrait;

class StoreRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'=>'required|exists:categories,id',
            'sub_category_id'=>'required|exists:categories,id',
            'name'=>'required|string',
            'price'=>'required|numeric',
            'offer_price'=>'numeric',
            'description'=>'string',
            'preparation_time'=>'string',
            'media'=>'required|array',
            'media.*'=>'required|mimes:jpeg,jpg,png'
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Object = new Product();
        $Object->setUserId($logged->getId());

        $Object->setVendorId($logged->vendor->getId());

        $Object->setCategoryId($this->category_id);
        $Object->setSubCategoryId($this->sub_category_id);
        $Object->setName($this->name);
        $Object->setPrice($this->price);
        $Object->setOfferPrice(@$this->offer_price);
        $Object->setDescription($this->description);
        $Object->setPreparationTime($this->preparation_time);
        $Object->save();
        $Object->refresh();
        if ($this->hasFile('media')){
            foreach ($this->file('media') as $media){
                $Media = new Media();
                $Media->setRefId($Object->getId());
                $Media->setFile($media);
                $Media->setType(Constant::MEDIA_TYPE['Product']);
                $Media->save();
            }
        }
        return $this->successJsonResponse([],new ProductResource($Object),'Product');
    }
}
