<?php

namespace App\Http\Requests\Api\Product;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Product\ProductResource;
use App\Models\Media;
use App\Models\Product;
use App\Traits\ResponseTrait;

class UpdateRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id'=>'required|exists:products,id',
            'category_id'=>'exists:categories,id',
            'sub_category_id'=>'exists:categories,id',
            'name'=>'string',
            'price'=>'numeric',
            'offer_price'=>'numeric',
            'description'=>'string',
            'preparation_time'=>'string',
            'media'=>'sometimes|array',
            'media.*'=>'sometimes|mimes:jpeg,jpg,png'

        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Object = (new Product())->find($this->product_id);
        if ($this->filled('category_id')){
            $Object->setCategoryId($this->category_id);
        }
        if ($this->filled('sub_category_id')) {
            $Object->setSubCategoryId($this->sub_category_id);
        }
        if ($this->filled('name')) {
            $Object->setName($this->name);
        }
        if ($this->filled('price')) {
            $Object->setPrice($this->price);
        }
        if ($this->filled('offer_price')) {
            $Object->setOfferPrice(@$this->offer_price);
        }
        if ($this->filled('description')) {
            $Object->setDescription($this->description);
        }
        if ($this->filled('preparation_time')) {
            $Object->setPreparationTime($this->preparation_time);
        }
        $Object->save();
        if ($this->hasFile('media')){
            foreach ($this->file('media') as $media){
                $Media = new Media();
                $Media->setRefId($Object->getId());
                $Media->setFile($media);
                $Media->setType(Constant::MEDIA_TYPE['Product']);
                $Media->save();
            }
        }
        return $this->successJsonResponse([],new ProductResource($Object),'Product');
    }
}
