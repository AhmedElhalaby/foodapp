<?php

namespace App\Http\Requests\Api\Product;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Product\ProductResource;
use App\Models\Media;
use App\Models\Product;
use App\Traits\ResponseTrait;

class UploadImageRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id'=>'required|exists:products,id',
            'media'=>'required|mimes:jpeg,jpg,png'
        ];
    }

    public function persist()
    {
        $Object = (new Product())->find($this->product_id);
        if ($this->hasFile('media')){
            $Media = new Media();
            $Media->setRefId($Object->getId());
            $Media->setFile($this->file('media'));
            $Media->setType(Constant::MEDIA_TYPE['Product']);
            $Media->save();
        }
        return $this->successJsonResponse([],new ProductResource($Object),'Product');
    }
}
