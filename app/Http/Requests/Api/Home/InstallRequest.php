<?php

namespace App\Http\Requests\Api\Home;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Home\BankAccountResource;
use App\Http\Resources\Api\General\CategoryResource;
use App\Http\Resources\Api\General\CityResource;
use App\Http\Resources\Api\Home\SubscriptionResource;
use App\Models\BankAccount;
use App\Models\Category;
use App\Models\City;
use App\Models\Setting;
use App\Models\Subscription;
use App\Traits\ResponseTrait;

class InstallRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function persist()
    {
        $data = [];
        $data['Cities'] = CityResource::collection(City::where('is_active',true)->get());
        $data['Categories'] = CategoryResource::collection(Category::where('is_active',true)->where('parent_id',null)->get());
        $data['Subscriptions'] = SubscriptionResource::collection(Subscription::where('is_active',true)->get());
        $data['Settings'] = Setting::pluck((app()->getLocale() =='en')?'value':'value_ar','key')->toArray();
        $data['BankAccounts'] = BankAccountResource::collection(BankAccount::all());
        $data['Essentials'] = [
            'UserType'=>Constant::USER_TYPE,
            'NotificationType'=>Constant::NOTIFICATION_TYPE,
            'VerificationType'=>Constant::VERIFICATION_TYPE,
            'VendorType'=>Constant::VENDOR_TYPE,
            'AdvertisementType'=>Constant::ADVERTISEMENT_TYPES,
            'AdvertisementStatus'=>Constant::ADVERTISEMENT_STATUSES,
            'OrderStatus'=>Constant::ORDER_STATUSES,
        ];
        return $this->successJsonResponse([],$data);
    }
}
