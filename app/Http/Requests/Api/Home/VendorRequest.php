<?php

namespace App\Http\Requests\Api\Home;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Home\VendorResource;
use App\Models\Product;
use App\Models\Vendor;
use App\Traits\ResponseTrait;

/**
 * @property mixed category_id
 * @property mixed sub_category_id
 * @property mixed type
 * @property mixed q
 * @property mixed per_page
 */
class VendorRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'sometimes|in:'.Constant::VENDOR_TYPE_RULES,
            'category_id' => 'sometimes|exists:categories,id',
            'sub_category_id' => 'sometimes|exists:categories,id',
        ];
    }

    public function persist()
    {
        $Objects = new Vendor();

        if($this->filled('category_id')){
            $Products = Product::where('category_id',$this->category_id)->pluck('vendor_id');
            $Objects = $Objects->whereIn('id',$Products);
        }
        if($this->filled('sub_category_id')){
            $Products = Product::where('sub_category_id',$this->sub_category_id)->pluck('vendor_id');
            $Objects = $Objects->whereIn('id',$Products);
        }
        if($this->filled('type')){
            $Objects = $Objects->where('type',$this->type);
        }
        if($this->filled('q')){
            $Objects = $Objects->where('name','LIKE',"%{$this->q}%");
        }
        $Objects = $Objects->orderBy('created_at','desc')->paginate($this->per_page?:10);
        return $this->successJsonResponse([],VendorResource::collection($Objects->items()),'Vendors',$Objects);
    }
}
