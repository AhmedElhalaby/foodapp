<?php

namespace App\Http\Requests\Api\Home;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Models\VendorSubscription;
use App\Traits\ResponseTrait;

/**
 * @property mixed subscription_id
 */
class SubscribeRequest extends ApiRequest
{
    use ResponseTrait;


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subscription_id'=>'required|exists:subscriptions,id',
            'file'=>'required_if:payment_method,'.Constant::SUBSCRIPTION_PAYMENT_METHOD['BankTransfer'].'|mimes:jpeg,jpg,png',
            'payment_method'=>'required|in:'.Constant::SUBSCRIPTION_PAYMENT_METHOD_RULES
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Object =new  VendorSubscription();
        $Object->setUserId($logged->getId());
        $Object->setVendorId($logged->vendor->getId());
        $Object->setSubscriptionId($this->subscription_id);
        if ($this->payment_method == Constant::SUBSCRIPTION_PAYMENT_METHOD['BankTransfer']){
            $Object->setFile($this->file('file'));
        }
        $Object->save();
        return $this->successJsonResponse([__('messages.saved_successfully')]);
    }
}
