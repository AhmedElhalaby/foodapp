<?php

namespace App\Http\Requests\Api\Home;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Home\VendorResource;
use App\Models\Product;
use App\Models\Vendor;
use App\Traits\ResponseTrait;

/**
 * @property mixed vendor_id
 */
class ShowVendorRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'sometimes|in:'.Constant::VENDOR_TYPE_RULES,
            'category_id' => 'sometimes|exists:categories,id',
            'vendor_id' => 'required|exists:vendors,id',
        ];
    }

    public function persist()
    {
        $Object = (new Vendor())->find($this->vendor_id);
        return $this->successJsonResponse([],new VendorResource($Object),'Vendor');
    }
}
