<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Order\OrderResource;
use App\Models\Order;
use App\Traits\ResponseTrait;

class UpdateRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'=>'required|exists:orders,id',
            'status'=>'required|in:'.Constant::ORDER_STATUSES_RULES,
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Object = (new Order())->find($this->order_id);
        Functions::CheckOrderSequence($Object->getStatus(),$this->status);
        switch ($this->status){
            case Constant::ORDER_STATUSES['Approved']:{
                $Object->setStatus(Constant::ORDER_STATUSES['Approved']);
                $Object->save();
                Functions::SendNotification($Object->user,'Order Approved',' You Order has been approved !','طلبك تم قبوله','لقد تم قبول طلبك ',$Object->getId(),Constant::NOTIFICATION_TYPE['Order'],true);
                break;
            }
            case Constant::ORDER_STATUSES['Rejected']:{
                $Object->setStatus(Constant::ORDER_STATUSES['Rejected']);
                $Object->save();
                Functions::SendNotification($Object->user,'Order Rejected',' You Order has been rejected !','طلبك تم رفضه','لقد تم رفض طلبك ',$Object->getId(),Constant::NOTIFICATION_TYPE['Order'],true);
                break;
            }
            case Constant::ORDER_STATUSES['Canceled']:{
                $Object->setStatus(Constant::ORDER_STATUSES['Canceled']);
                $Object->save();
                Functions::SendNotification($Object->vendor->user,'Order Canceled',' The Order has been Canceled !','الطلب تم الغاءه','لقد تم الغاء الطلب ',$Object->getId(),Constant::NOTIFICATION_TYPE['Order'],true);
                break;
            }
            case Constant::ORDER_STATUSES['Completed']:{
                $Object->setStatus(Constant::ORDER_STATUSES['Completed']);
                $Object->save();
                break;
            }
        }
        $Object->save();
        return $this->successJsonResponse([],new OrderResource($Object),'Order');
    }
}
