<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Order\OrderResource;
use App\Models\Order;
use App\Traits\ResponseTrait;

class IndexRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Objects = new Order();
        if($logged->getType() == Constant::USER_TYPE['Customer']){
            $Objects = $Objects->where('user_id',$logged->getId());
        }else{
            $Objects = $Objects->where('vendor_id',$logged->vendor->getId());
        }
        if($this->filled('status')){
            $Objects = $Objects->where('status',$this->status);
        }
        if($this->filled('is_complete')){
            if ($this->is_complete == 1){
                $Objects = $Objects->whereIn('status',[Constant::ORDER_STATUSES['Canceled'],Constant::ORDER_STATUSES['Rejected'],Constant::ORDER_STATUSES['Completed']]);
            }else{
                $Objects = $Objects->whereIn('status',[Constant::ORDER_STATUSES['Approved'],Constant::ORDER_STATUSES['Pending']]);
            }
        }
        $Objects = $Objects->orderBy('created_at','desc')->paginate($this->per_page?:10);
        return $this->successJsonResponse([],OrderResource::collection($Objects->items()),'Orders',$Objects);
    }
}
