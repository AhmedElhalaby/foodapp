<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Requests\Api\ApiRequest;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Models\Vendor;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

/**
 * @property mixed lng
 * @property mixed lat
 * @property mixed recipient_name
 * @property mixed address
 * @property mixed notes
 * @property mixed order_products
 */
class StoreRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'recipient_name'=>'required|string',
            'address'=>'required|string',
            'lat'=>'required|string',
            'lng'=>'required|string',
            'order_date'=>'required|date',
            'order_time'=>'required|date_format:H:i:s',
            'notes'=>'sometimes|string',
            'order_products'=>'required|array',
            'order_products.*.product_id'=>'required|exists:products,id',
            'order_products.*.quantity'=>'required|numeric',
            'order_products.*.note'=>'sometimes|string',
        ];
    }

    public function persist()
    {
        try {
            DB::beginTransaction();
            $ProductCost = 0;
            $logged = auth()->user();
            $order_products = collect($this->order_products);
            $VendorProducts = Product::findMany($order_products->pluck('product_id'))->groupBy('vendor_id');
            foreach ($VendorProducts as $key => $Products) {
                $Vendor = (new Vendor())->find($key);
                $DeliveryCost = $Vendor->getDeliveryPrice()??0;
                $Order = new Order();
                $Order->setUserId($logged->getId());
                $Order->setVendorId($Vendor->getId());
                $Order->setRecipientName($this->recipient_name);
                $Order->setLng($this->lng);
                $Order->setLat($this->lat);
                $Order->setOrderDate($this->order_date);
                $Order->setOrderTime($this->order_time);
                $Order->setAddress($this->address);
                $Order->setNotes(@$this->notes);
                $Order->setDeliveryCost($DeliveryCost);
                $Order->save();
                $Order->refresh();
                foreach ($Products as $Product) {
                    $OrderProduct = (object)$order_products->where('product_id', $Product->id)->first();
                    if($OrderProduct){
                        $Object = new OrderProduct();
                        $Object->setOrderId($Order->getId());
                        $Object->setProductId($Product->getId());
                        $Object->setName($Product->getName());
                        $Object->setDescription($Product->getDescription());
                        $Object->setCategoryId($Product->getCategoryId());
                        $Object->setSubCategoryId($Product->getSubCategoryId());
                        $Object->setPreparationTime($Product->getPreparationTime());
                        $Object->setPrice($Product->getPrice());
                        $Object->setOfferPrice($Product->getOfferPrice());
                        $Object->setQuantity($OrderProduct->quantity);
                        $Object->setNote(@$OrderProduct->note);
                        $Image = $Product->media()->first()?$Product->media()->first()->getFile():'';
                        $Object->setImage($Image);
                        $Object->save();
                        $ProductCost += $OrderProduct->quantity*$Product->getPrice();
                    }
                    $Order->setProductsCost($ProductCost);
                    $Order->setTotalCost(($ProductCost+$DeliveryCost));
                    $Order->save();
                }
                Functions::SendNotification($Vendor->user,'New Order',' You have new Order','طلب جديد','لديك طلب جديد',$Order->getId(),Constant::NOTIFICATION_TYPE['Order'],true);
            }
            DB::commit();
            return $this->successJsonResponse([__('messages.created_successful')]);
        }catch (\Exception $exception){
            DB::rollback();
            return $this->failJsonResponse([__('messages.wrong_data').' '.$exception->getMessage()]);
        }
    }
}
