<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Order\OrderResource;
use App\Models\Order;
use App\Models\Product;
use App\Models\Review;
use App\Traits\ResponseTrait;

class ReviewRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'=>'required|exists:orders,id',
            'rate'=>'required|numeric|max:5|min:1',
            'ProductReview'=>'array',
            'ProductReview.*.product_id'=>'required|exists:products,id',
            'ProductReview.*.rate'=>'required|numeric|max:5|min:1',
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Order = (new Order())->find($this->order_id);
        $Object = new Review();
        if($Object->where('type',Constant::REVIEW_TYPES['Order'])->where('user_id',$logged->getId())->where('ref_id',$Order->getId())->first()){
            $Object = $Object->where('type',Constant::REVIEW_TYPES['Order'])->where('user_id',$logged->getId())->where('ref_id',$Order->getId())->first();
        }else{
            $Object = new Review();
            $Object->setUserId($logged->getId());
            $Object->setRefId($Order->getId());
            $Object->setType(Constant::REVIEW_TYPES['Order']);

        }
        $Object->setRate($this->rate);
        $Object->setReview(@$this->review);
        $Object->save();
        if ($this->filled('ProductReview')){
            foreach ($this->ProductReview as $ProductReview){
                $Product = (new Product())->find($ProductReview['product_id']);
                $Object = new Review();
                if($Object->where('type',Constant::REVIEW_TYPES['Product'])->where('user_id',$logged->getId())->where('ref_id',$Product->getId())->first()){
                    $Object = $Object->where('type',Constant::REVIEW_TYPES['Product'])->where('user_id',$logged->getId())->where('ref_id',$Product->getId())->first();
                }else{
                    $Object = new Review();
                    $Object->setUserId($logged->getId());
                    $Object->setRefId($Product->getId());
                    $Object->setType(Constant::REVIEW_TYPES['Product']);

                }
                $Object->setRate($ProductReview['rate']);
                $Object->setReview(@$ProductReview['review']);
                $Object->save();
            }
        }
        return $this->successJsonResponse([],new OrderResource($Order),'Order');
    }
}
