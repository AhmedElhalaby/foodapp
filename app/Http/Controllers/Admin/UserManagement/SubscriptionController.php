<?php

namespace App\Http\Controllers\Admin\UserManagement;

use App\Helpers\Constant;
use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\UserManagement\Subscription\ApproveRequest;
use App\Http\Requests\Admin\UserManagement\Subscription\RejectRequest;
use App\Models\Subscription;
use App\Models\User;
use App\Models\VendorSubscription;
use App\Traits\AhmedPanelTrait;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class SubscriptionController extends Controller
{
    use AhmedPanelTrait;

    public function setup()
    {
        $this->setRedirect('admin/user_managements/subscriptions');
        $this->setEntity(new VendorSubscription());
        $this->setCreate(false);
        $this->setExport(false);
        $this->setTable('vendor_subscriptions');
        $this->setLang('VendorSubscription');
        $this->setColumns([
            'user_id'=> [
                'name'=>'user_id',
                'type'=>'custom_relation',
                'relation'=>[
                    'data'=> User::all(),
                    'custom'=>function($Object){
                        return $Object->name;
                    },
                    'entity'=>'user'
                ],
                'is_searchable'=>true,
                'order'=>true
            ],
            'subscription_id'=> [
                'name'=>'subscription_id',
                'type'=>'custom_relation',
                'relation'=>[
                    'data'=> Subscription::all(),
                    'custom'=>function($Object){
                        return $Object->name;
                    },
                    'entity'=>'subscription'
                ],
                'is_searchable'=>true,
                'order'=>true
            ],
            'file'=> [
                'name'=>'file',
                'type'=>'file',
                'is_searchable'=>true,
                'order'=>true
            ],
            'status'=> [
                'name'=>'status',
                'type'=>'select',
                'data'=>[
                    Constant::SUBSCRIPTION_STATUSES['Pending'] =>__('crud.VendorSubscription.Statuses.'.Constant::SUBSCRIPTION_STATUSES['Pending'],[],session('my_locale')),
                    Constant::SUBSCRIPTION_STATUSES['Approved'] =>__('crud.VendorSubscription.Statuses.'.Constant::SUBSCRIPTION_STATUSES['Approved'],[],session('my_locale')),
                    Constant::SUBSCRIPTION_STATUSES['Rejected'] =>__('crud.VendorSubscription.Statuses.'.Constant::SUBSCRIPTION_STATUSES['Rejected'],[],session('my_locale')),
                ],
                'is_searchable'=>true,
                'order'=>true
            ],
        ]);
        $this->SetLinks([
            'approve'=>[
                'route'=>'approve',
                'icon'=>'fa-check-square',
                'lang'=>__('crud.VendorSubscription.Links.approve'),
                'condition'=>function ($Object){
                    return ($Object->getStatus() == Constant::SUBSCRIPTION_STATUSES['Pending']);
                }
            ],
            'reject'=>[
                'route'=>'reject',
                'icon'=>'fa-window-close',
                'lang'=>__('crud.VendorSubscription.Links.reject'),
                'condition'=>function ($Object){
                    return ($Object->getStatus() == Constant::ADVERTISEMENT_STATUSES['Pending']);
                }
            ],
        ]);
    }

    /**
     * @param ApproveRequest $request
     * @param $id
     * @return Application|RedirectResponse|Redirector
     */
    public function approve(ApproveRequest $request, $id){
        return $request->preset($this,$id);
    }

    /**
     * @param RejectRequest $request
     * @param $id
     * @return Application|RedirectResponse|Redirector
     */
    public function reject(RejectRequest $request, $id){
        return $request->preset($this,$id);
    }


}
