<?php

namespace App\Http\Controllers\Admin\AppData;

use App\Helpers\Constant;
use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\AppData\Advertisement\ActivateRequest;
use App\Http\Requests\Admin\AppData\Advertisement\DeActivateRequest;
use App\Models\Advertisement;
use App\Models\User;
use App\Traits\AhmedPanelTrait;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class AdvertisementController extends Controller
{
    use AhmedPanelTrait;

    public function setup()
    {
        $this->setRedirect('admin/app_data/advertisements');
        $this->setEntity(new Advertisement());
        $this->setTable('advertisements');
        $this->setLang('Advertisement');
        $this->setCreate(false);
        $this->setColumns([
            'media'=>[
                'name'=>'media',
                'type'=>'image',
                'is_searchable'=>false,
                'order'=>false,
            ],
            'user_id'=> [
                'name'=>'user_id',
                'type'=>'relation',
                'relation'=>[
                    'data'=> User::all(),
                    'name'=>'name',
                    'entity'=>'user'
                ],
                'is_searchable'=>true,
                'order'=>true
            ],
            'type'=> [
                'name'=>'type',
                'type'=>'select',
                'data'=>[
                    Constant::ADVERTISEMENT_TYPES['Product'] =>__('crud.Advertisement.Types.'.Constant::ADVERTISEMENT_TYPES['Product']),
                    Constant::ADVERTISEMENT_TYPES['Category'] =>__('crud.Advertisement.Types.'.Constant::ADVERTISEMENT_TYPES['Category']),
                    Constant::ADVERTISEMENT_TYPES['Store'] =>__('crud.Advertisement.Types.'.Constant::ADVERTISEMENT_TYPES['Store']),
                ],
                'is_searchable'=>true,
                'order'=>true
            ],
            'status'=> [
                'name'=>'status',
                'type'=>'select',
                'data'=>[
                    Constant::ADVERTISEMENT_STATUSES['Pending'] =>__('crud.Advertisement.Statuses.'.Constant::ADVERTISEMENT_STATUSES['Pending']),
                    Constant::ADVERTISEMENT_STATUSES['Active'] =>__('crud.Advertisement.Statuses.'.Constant::ADVERTISEMENT_STATUSES['Active']),
                    Constant::ADVERTISEMENT_STATUSES['NotActive'] =>__('crud.Advertisement.Statuses.'.Constant::ADVERTISEMENT_STATUSES['NotActive']),
                ],
                'is_searchable'=>true,
                'order'=>true
            ],
        ]);
        $this->SetLinks([
            'activate'=>[
                'route'=>'activate',
                'icon'=>'fa-check-square',
                'lang'=>__('crud.Advertisement.Links.activate'),
                'condition'=>function ($Object){
                    return ($Object->getStatus() == Constant::ADVERTISEMENT_STATUSES['Pending'] || $Object->getStatus() == Constant::ADVERTISEMENT_STATUSES['NotActive']);
                }
            ],
            'deactivate'=>[
                'route'=>'deactivate',
                'icon'=>'fa-window-close',
                'lang'=>__('crud.Advertisement.Links.deactivate'),
                'condition'=>function ($Object){
                    return ($Object->getStatus() == Constant::ADVERTISEMENT_STATUSES['Active']);
                }
            ],
        ]);
    }

    /**
     * @param ActivateRequest $request
     * @param $id
     * @return Application|RedirectResponse|Redirector
     */
    public function activate(ActivateRequest $request, $id){
        return $request->preset($this,$id);
    }

    /**
     * @param DeActivateRequest $request
     * @param $id
     * @return Application|RedirectResponse|Redirector
     */
    public function deactivate(DeActivateRequest $request, $id){
        return $request->preset($this,$id);
    }
}
