<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Advertisement\IndexRequest;
use App\Http\Requests\Api\Advertisement\MineRequest;
use App\Http\Requests\Api\Advertisement\MyProductRequest;
use App\Http\Requests\Api\Advertisement\StoreRequest;
use App\Http\Requests\Api\Advertisement\ShowRequest;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class AdvertisementController extends Controller
{
    use ResponseTrait;

    /**
     * @param IndexRequest $request
     * @return JsonResponse
     */
    public function index(IndexRequest $request){
        return $request->persist();
    }

    /**
     * @param MyProductRequest $request
     * @return JsonResponse
     */
    public function products(MyProductRequest $request){
        return $request->persist();
    }

    /**
     * @param MineRequest $request
     * @return JsonResponse
     */
    public function mine(MineRequest $request){
        return $request->persist();
    }

    /**
     * @param StoreRequest $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request){
        return $request->persist();
    }
    /**
     * @param ShowRequest $request
     * @return JsonResponse
     */
    public function show(ShowRequest $request){
        return $request->persist();
    }

}
