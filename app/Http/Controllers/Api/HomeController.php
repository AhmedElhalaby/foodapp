<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Home\AddTicketRequest;
use App\Http\Requests\Api\Home\AddTicketResponseRequest;
use App\Http\Requests\Api\Home\FaqRequest;
use App\Http\Requests\Api\Home\InstallRequest;
use App\Http\Requests\Api\Home\SubscribeRequest;
use App\Http\Requests\Api\Home\VendorRequest;
use App\Http\Requests\Api\Home\ShowVendorRequest;
use App\Http\Requests\Api\Home\ShowTicketRequest;
use App\Http\Requests\Api\Home\TicketRequest;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class HomeController extends Controller
{
    use ResponseTrait;

    /**
     * @param InstallRequest $request
     * @return JsonResponse
     */
    public function install(InstallRequest $request){
        return $request->persist();
    }

    /**
     * @param VendorRequest $request
     * @return JsonResponse
     */
    public function vendors(VendorRequest $request){
        return $request->persist();
    }


    /**
     * @param ShowVendorRequest $request
     * @return JsonResponse
     */
    public function vendor(ShowVendorRequest $request){
        return $request->persist();
    }

    /**
     * @param TicketRequest $request
     * @return JsonResponse
     */
    public function tickets(TicketRequest $request){
        return $request->persist();
    }
    /**
     * @param ShowTicketRequest $request
     * @return JsonResponse
     */
    public function show_ticket(ShowTicketRequest $request){
        return $request->persist();
    }
    /**
     * @param AddTicketRequest $request
     * @return JsonResponse
     */
    public function addTicket(AddTicketRequest $request){
        return $request->persist();
    }
    /**
     * @param AddTicketResponseRequest $request
     * @return JsonResponse
     */
    public function add_response(AddTicketResponseRequest $request){
        return $request->persist();
    }

    /**
     * @param FaqRequest $request
     * @return JsonResponse
     */
    public function faqs(FaqRequest $request)
    {
        return $request->persist();
    }
    /**
     * @param SubscribeRequest $request
     * @return JsonResponse
     */
    public function subscribe(SubscribeRequest $request)
    {
        return $request->persist();
    }
}
