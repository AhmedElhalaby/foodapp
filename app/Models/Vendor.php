<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property mixed user_id
 * @property mixed name
 * @property mixed identity_number
 * @property mixed commercial_registration_no
 * @property mixed manager_name
 * @property mixed delivery_price
 * @property mixed type
 * @property boolean is_open
 * @method Vendor find(int $id)
 * @method static updateOrCreate(array $array, array $array1)
 * @method Vendor where($column, $operator = null, $value = null, $boolean = 'and')
 * @method orderBy($column, $direction = 'asc')
 * @method whereIn($column, $values, $boolean = 'and', $not = false)
 */

class Vendor extends Model
{
    protected $table = 'vendors';
    protected $fillable = ['user_id','name','identity_number','commercial_registration_no','manager_name','delivery_price','type','is_open'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function products(){
        return $this->hasMany(Product::class,'vendor_id');
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getIdentityNumber()
    {
        return $this->identity_number;
    }

    /**
     * @param mixed $identity_number
     */
    public function setIdentityNumber($identity_number): void
    {
        $this->identity_number = $identity_number;
    }

    /**
     * @return mixed
     */
    public function getCommercialRegistrationNo()
    {
        return $this->commercial_registration_no;
    }

    /**
     * @param mixed $commercial_registration_no
     */
    public function setCommercialRegistrationNo($commercial_registration_no): void
    {
        $this->commercial_registration_no = $commercial_registration_no;
    }

    /**
     * @return mixed
     */
    public function getManagerName()
    {
        return $this->manager_name;
    }

    /**
     * @param mixed $manager_name
     */
    public function setManagerName($manager_name): void
    {
        $this->manager_name = $manager_name;
    }

    /**
     * @return mixed
     */
    public function getDeliveryPrice()
    {
        return $this->delivery_price;
    }

    /**
     * @param mixed $delivery_price
     */
    public function setDeliveryPrice($delivery_price): void
    {
        $this->delivery_price = $delivery_price;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isIsOpen(): bool
    {
        return $this->is_open;
    }

    /**
     * @param bool $is_open
     */
    public function setIsOpen(bool $is_open): void
    {
        $this->is_open = $is_open;
    }

}
