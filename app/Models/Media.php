<?php

namespace App\Models;

use App\Helpers\Functions;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property integer ref_id
 * @property integer type
 * @property string file
 * @method Media find(int $id)
 * @method static updateOrCreate(array $array, array $array1)
 */
class Media extends Model
{
    protected $table = 'media';
    protected $fillable = ['ref_id','type','file'];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getRefId(): int
    {
        return $this->ref_id;
    }

    /**
     * @param int $ref_id
     */
    public function setRefId(int $ref_id): void
    {
        $this->ref_id = $ref_id;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return asset($this->file);
    }

    /**
     * @param mixed $file
     */
    public function setFile($file): void
    {
        $this->file = Functions::StoreImageModel($file,'products');
    }

}
