<?php

namespace App\Models;

use App\Helpers\Functions;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property integer ref_id
 * @property integer user_id
 * @property integer type
 * @property mixed media
 * @property integer status
 * @method Advertisement find(int $id)
 * @method static updateOrCreate(array $array, array $array1)
 */
class Advertisement extends Model
{
    protected $table = 'advertisements';
    protected $fillable = ['ref_id','user_id','status','type','media'];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getRefId(): int
    {
        return $this->ref_id;
    }

    /**
     * @param int $ref_id
     */
    public function setRefId(int $ref_id): void
    {
        $this->ref_id = $ref_id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getMedia()
    {
        return asset($this->media);
    }

    /**
     * @param mixed $media
     */
    public function setMedia($media): void
    {
        $this->media = Functions::StoreImageModel($media,'advertisements');
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

}
