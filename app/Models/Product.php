<?php

namespace App\Models;

use App\Helpers\Constant;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property mixed user_id
 * @property mixed vendor_id
 * @property mixed name
 * @property mixed category_id
 * @property mixed sub_category_id
 * @property float price
 * @property float offer_price
 * @property mixed description
 * @property mixed preparation_time
 * @method Product find(int $id)
 * @method static updateOrCreate(array $array, array $array1)
 * @method static where($column, $operator = null, $value = null, $boolean = 'and')
 * @method static findMany($ids, $columns = ['*'])
 */
class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['user_id','vendor_id','name','category_id','sub_category_id','price','offer_price','description','preparation_time'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function vendor(){
        return $this->belongsTo(Vendor::class);
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function sub_category(){
        return $this->belongsTo(Category::class);
    }
    public function media(){
        return $this->hasMany(Media::class,'ref_id','id')->where('type',Constant::MEDIA_TYPE['Product']);
    }
    public function reviews(){
        return $this->hasMany(Review::class,'ref_id','id')->where('type',Constant::REVIEW_TYPES['Product']);
    }
    public function rate(){
        return ($this->reviews()->count()>0)?round($this->reviews()->avg('rate')):0;
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getVendorId()
    {
        return $this->vendor_id;
    }

    /**
     * @param mixed $vendor_id
     */
    public function setVendorId($vendor_id): void
    {
        $this->vendor_id = $vendor_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param mixed $category_id
     */
    public function setCategoryId($category_id): void
    {
        $this->category_id = $category_id;
    }

    /**
     * @return mixed
     */
    public function getSubCategoryId()
    {
        return $this->sub_category_id;
    }

    /**
     * @param mixed $sub_category_id
     */
    public function setSubCategoryId($sub_category_id): void
    {
        $this->sub_category_id = $sub_category_id;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return round($this->price,2).'';
    }

    /**
     * @param float $price
     */
    public function setPrice($price): void
    {
        $this->price = round($price,2);
    }

    /**
     * @return float
     */
    public function getOfferPrice()
    {
        return round($this->offer_price,2).'';
    }

    /**
     * @param float $offer_price
     */
    public function setOfferPrice($offer_price): void
    {
        $this->offer_price = round($offer_price,2);
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPreparationTime()
    {
        return $this->preparation_time;
    }

    /**
     * @param mixed $preparation_time
     */
    public function setPreparationTime($preparation_time): void
    {
        $this->preparation_time = $preparation_time;
    }

}
