<?php

namespace App\Models;

use App\Helpers\Functions;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property integer vendor_id
 * @property integer user_id
 * @property integer subscription_id
 * @property mixed file
 * @property mixed expire_at
 * @property integer status
 * @method VendorSubscription find(int $id)
 * @method static updateOrCreate(array $array, array $array1)
 */
class VendorSubscription extends Model
{
    protected $table = 'vendor_subscriptions';
    protected $fillable = ['vendor_id','user_id','subscription_id','file','expire_at','status'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function vendor(){
        return $this->belongsTo(Vendor::class);
    }
    public function subscription(){
        return $this->belongsTo(Subscription::class);
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getVendorId(): int
    {
        return $this->vendor_id;
    }

    /**
     * @param int $vendor_id
     */
    public function setVendorId(int $vendor_id): void
    {
        $this->vendor_id = $vendor_id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getSubscriptionId(): int
    {
        return $this->subscription_id;
    }

    /**
     * @param int $subscription_id
     */
    public function setSubscriptionId(int $subscription_id): void
    {
        $this->subscription_id = $subscription_id;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file): void
    {
        $this->file = Functions::StoreImageModel($file,'vendor_subscriptions');
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getExpireAt()
    {
        return $this->expire_at;
    }

    /**
     * @param mixed $expire_at
     */
    public function setExpireAt($expire_at): void
    {
        $this->expire_at = $expire_at;
    }

}
