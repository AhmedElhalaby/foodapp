<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property mixed user_id
 * @property mixed vendor_id
 * @property mixed recipient_name
 * @property mixed address
 * @property mixed products_cost
 * @property mixed total_cost
 * @property mixed delivery_cost
 * @property mixed order_date
 * @property mixed order_time
 * @property mixed lat
 * @property mixed lng
 * @property mixed status
 * @property mixed notes
 * @method Order find(int $id)
 * @method static updateOrCreate(array $array, array $array1)
 */
class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = ['user_id', 'vendor_id', 'recipient_name', 'address', 'products_cost', 'total_cost', 'delivery_cost','order_date','order_time', 'lat', 'lng', 'status', 'notes'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }
    public function order_products()
    {
        return $this->hasMany(OrderProduct::class);
    }
    public function review()
    {
        return $this->hasOne(Review::class,'ref_id');
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getVendorId()
    {
        return $this->vendor_id;
    }

    /**
     * @param mixed $vendor_id
     */
    public function setVendorId($vendor_id): void
    {
        $this->vendor_id = $vendor_id;
    }

    /**
     * @return mixed
     */
    public function getRecipientName()
    {
        return $this->recipient_name;
    }

    /**
     * @param mixed $recipient_name
     */
    public function setRecipientName($recipient_name): void
    {
        $this->recipient_name = $recipient_name;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }


    /**
     * @return mixed
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param mixed $lat
     */
    public function setLat($lat): void
    {
        $this->lat = $lat;
    }

    /**
     * @return mixed
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param mixed $lng
     */
    public function setLng($lng): void
    {
        $this->lng = $lng;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param mixed $notes
     */
    public function setNotes($notes): void
    {
        $this->notes = $notes;
    }

    /**
     * @return mixed
     */
    public function getProductsCost()
    {
        return $this->products_cost;
    }

    /**
     * @param mixed $products_cost
     */
    public function setProductsCost($products_cost): void
    {
        $this->products_cost = $products_cost;
    }

    /**
     * @return mixed
     */
    public function getTotalCost()
    {
        return $this->total_cost;
    }

    /**
     * @param mixed $total_cost
     */
    public function setTotalCost($total_cost): void
    {
        $this->total_cost = $total_cost;
    }

    /**
     * @return mixed
     */
    public function getDeliveryCost()
    {
        return $this->delivery_cost;
    }

    /**
     * @param mixed $delivery_cost
     */
    public function setDeliveryCost($delivery_cost): void
    {
        $this->delivery_cost = $delivery_cost;
    }

    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->order_date;
    }

    /**
     * @param mixed $order_date
     */
    public function setOrderDate($order_date): void
    {
        $this->order_date = $order_date;
    }

    /**
     * @return mixed
     */
    public function getOrderTime()
    {
        return $this->order_time;
    }

    /**
     * @param mixed $order_time
     */
    public function setOrderTime($order_time): void
    {
        $this->order_time = $order_time;
    }

}
