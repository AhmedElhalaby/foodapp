<?php


namespace App\Helpers;


class Constant
{
    const NOTIFICATION_TYPE = [
        'General'=>1,
        'Order'=>2,
        'Ticket'=>3,
    ];
    const MEDIA_TYPE = [
        'Product'=>1,
    ];
    const USER_TYPE = [
        'Customer'=>1,
        'Vendor'=>2
    ];
    const USER_TYPE_RULES = '1,2';

    const VENDOR_TYPE = [
        'Gallery'=>1,
        'Family'=>2
    ];
    const VENDOR_TYPE_RULES = '1,2';

    const VERIFICATION_TYPE = [
        'Email'=>1,
        'Mobile'=>2
    ];
    const VERIFICATION_TYPE_RULES = '1,2';

    const ADVERTISEMENT_TYPES = [
        'Product'=>1,
        'Store'=>2,
        'Category'=>3,
    ];
    const ADVERTISEMENT_STATUSES = [
        'Pending'=>0,
        'Active'=>1,
        'NotActive'=>2,
    ];
    const REVIEW_TYPES = [
        'Order'=>1,
        'Product'=>2,
    ];
    const SUBSCRIPTION_STATUSES = [
        'Pending'=>0,
        'Approved'=>1,
        'Rejected'=>2,
    ];
    const ORDER_STATUSES = [
        'Pending'=>0,
        'Approved'=>1,
        'Rejected'=>2,
        'Completed'=>3,
        'Canceled'=>4,
    ];
    const ORDER_STATUSES_RULES = '1,2,3,4';
    const TICKETS_STATUS = [
        'New'=>1,
        'Closed'=>2
    ];

    const SUBSCRIPTION_PAYMENT_METHOD = [
        'BankTransfer'=>1,
        'Cash'=>2,
    ];
    const SENDER_TYPE = [
        'User'=>1,
        'Admin'=>2,
    ];
    const SUBSCRIPTION_PAYMENT_METHOD_RULES = '1,2';

}
